# Pense bête git


## merge en CLI

```console
# créer une nouvelle branche et basculer dessus
$ git branch test_branch
$ git checkout test_branch

# même opération en une ligne
$ git checkout -b new_branch

# travailler sur la branche en question
$ ...

# basculer sur la branche principale
$ git checkout main
$ git merge test_branch

# supprimer la branche de travail
$ git branch -d test_branch
```
