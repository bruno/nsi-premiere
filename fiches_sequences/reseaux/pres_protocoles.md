

---

# Protocoles de communication


Un protocole de communication est un ensemble de règles précisant :

- le format des informations échangées
- la manière de les échanger
- la manière d'établir la communication et de la terminer

---

## Protocole TCP/IP

----

### **protocole IP** (Internet Protocol) 

C'est le protocole qui assure l’acheminement des paquets d’une `machine A` vers 
une `machine B`, en utilisant notamment les adresses IP de ces deux machines.

----

### **protocole TCP** (Transmission Control Protocol) 

C'est le protocole qui assure que la transmission des paquets entre deux machines 
se fait correctement (numérotation et accusés de réception).

----

### Les différentes étapes du protocole TCP/IP

----

** Étape 1 : Encapsulation **

 - TCP découpe et encapsule en donnant un numéro
 - IP encapsule en donnant une adresse IP

![](https://info-mounier.fr/premiere_nsi/archi_os/data/tcp_ip_1.png)

----

**Étape 2 : Transport**

- IP s'assure de la transmission
- les routeurs déterminent le chemin le plus court en fonction de l'IP

![](https://info-mounier.fr/premiere_nsi/archi_os/data/tcp_ip_2.png)

----

**Étape 3 : Désencapsulation**

- IP transmet les paquets à TCP
- TCP les reconstitue

![](https://info-mounier.fr/premiere_nsi/archi_os/data/tcp_ip_3.png)

----

**Étape 4 : Envoi de l'accusé de réception**

- TCP s'assure qu'un paquet est bien arrivé à destination. 
- ordinateur B envoie un accusé de réception.

![](https://info-mounier.fr/premiere_nsi/archi_os/data/tcp_ip_4.png)


----

- Si un des paquets n'arrive pas à destination, ou si l'ordinateur A ne reçoit pas cet accusé de réception en provenance de B, le fichier ne pourra pas être reconstitué.
- Le paquet "perdu" devra être renvoyé par l'émetteur au bout d'un temps prédéfini.

---- 

En réalité :

- plus complexe
- plus de "couches"

---

## Le modèle de couches Internet

----

Une pile de protocoles :
- succession de plusieurs couches de protocoles. 
- "pile" : chaque couche de protocole s'appuie sur celles qui sont au-dessus afin d'y apporter un supplément de fonctionnalité.

----

![](https://info-mounier.fr/premiere_nsi/archi_os/data/modele_internet_4_couches.svg)

----

### Couche application 

c'est le point d'accès aux services réseau pour les utilisateurs : elle met en oeuvre des protocoles permettant de coder et décoder les données utiles pour les applications (par exemple, le codage d'un email à envoyer)

----

### Couche transport 

Elle gère les connexions entre les applications : elle met en oeuvre des protocoles chargés du contrôle des flux réseaux et de la gestion des erreurs (synchronisation des deux machines, numérotation des paquets, accusés de réception)

----

### Couche internet 

Elle met en oeuvre les protocoles permettant d'acheminer les paquets jusqu'à destination via différents routeurs à travers le réseau.

----

### Couche accès réseau 

Elle met en oeuvre les protocoles permettant de transférer physiquement les données entre deux machines d'un même réseau (via Ethernet, Wi-Fi, fibre, etc.)
