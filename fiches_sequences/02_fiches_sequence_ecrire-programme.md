---
title : Fiche séquence - Ecrire un programme
---

## Positionnement

- niveau : première
- dans la progression : 
  - 2e séquence de l'année
  - fait suite à "Ecrire un algorithme"

## Objectifs 

- découvrir les outils de programmation  
- découvrir/ réviser la syntaxe élémentaire de python  
- traduire un algorithme en programme python  
- exécuter un programme python  

## Contenu

### Prérequis

- notion d'algorithme
- lire et interpréter un algorithme écrit en pseudo code  

### Activités

- pyrates pour réintroduire les notions et vérifier le niveau des élèves.
- utiliser la console python
- traduire un algorithme en programme avec un éditeur de texte :
  - basique : blocnote
  - élaboré : sublime texte / VScode
- utiliser un IDE 


### Contenu de cours

- origine de python
- installation
- utilisation de la ligne de commande
- utilisation d'un IDE
-> https://cgouygou.github.io/1NSI/T06_Python/T6.1_Python/T6.1_0_Python/ 

- syntaxe de basique
- opérateurs mathématiques
- type de base : entier, flottant, booleen, chaines de caractères
- entrées/sortie
- variables, boucles, conditions (initiation)


## Évaluation

## Bilan
