# NSI_premiere

Dépôt pour l'organisation, les activités, les exercices, les évaluations et 
les TP de l'enseignement de NSI en première au lycée de l'Emperi pour l'année 
2023-2024.

- organisation : chapitrage, progression, gestion des projets
- évaluations : QCM et évaluations sommatives
- TP : notebook Jupyter (pour Capytale)
- exercices : fiches d'exercices, questions Moodle
- activités : fiches d'activités (mise en situation , débranchée)
-  ...
