---
title: "Réseaux et protocoles : Architecture des réseaux"
author: "d'après Germain BECKER, Lycée Mounier, ANGERS"
date: "2023-09-04"
subject: "Réseaux et protocoles"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...


# Exercice 1

1. Un réseau possède un seul ordinateur d'adresse IP 192.168.5.17/24. L'administrateur 
réseau doit en installer 3 autres. 

Donner une adresse IP valide pour chacun d'eux.

2. On considère le réseau ci-dessous formé de deux sous-réseaux de masque /24. Le routeur
qui interconnecte les deux sous-réseaux possède une adresse IP dans chacun d'eux. 

Donner une @IP possible pour ce routeur dans chaque réseau.

![](ex1_q2.png){width=600px}

# Exercice 2

On donne les adresses IP de différentes machines :

- `147.12.1.24/16`
- `192.168.2.45/24`
- `5.23.65.87/8`
- `25.8.254.253/20`

1. Déterminer pour chacune les adresses des réseaux.  

2. Déterminez pour chacune les adresses de broadcast.
   
\newpage
   
# Exercice 3

1. Combien de machines peut-on connecter sur un réseau dont le masque est 255.0.0.0 ?

2. Combien de machines peut-on connecter sur un réseau dont le masque est /20 ?

3. On installe 10 machines sur un réseau local. L'adresse IP 172.16.29.35 avec le masque 255.255.255.240 (soit /28) est attribuée à une de ces machines.

a. Combien d'hôtes peut-on placer dans le réseau où est située cette machine ?  
b. Dans quel réseau (préciser l'adresse) sont situées ces machines ?  
c. Pour ce réseau, donnez la première et la dernière adresse IP valide pour ces machines.
    Quelle est l'adresse de diffusion (broadcast) pour ce réseau ?

4. Soit 2 machines A et B connectées à un switch. Parmi les cas ci-dessous, indiquer celui 
   ou ceux pour pourront communiquer ensemble.

|Adresse de A | Adresse de B |
|:---:|:---:|
| 172.23.4.7/16 | 172.23.5.8/16|
| 24.2.8.127/8 | 24.23.5.52/8 |
| 193.28.7.2/24 | 193.28.8.3/24 |
|145.12.1.24/16 |145.12.2.28/8|

