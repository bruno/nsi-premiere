from doctest import testmod

#EX1
def total_str(n):
    """
    >>> total_str(42)
    'total: 42'
    """
    return "total: " + str(n)


def somme(a,b):
    """
    >>> somme(40, 2)
    '40+2=42'
    >>> somme(1, 4)
    '1+4=5'
    """
    return str(a) + '+' + str(b) + '=' + str(a+b)


#EX2
def n_nb(n):
    """
    >>> print(n_nb(4))
    4444
    >>> print(n_nb(1))
    1
    >>> print(n_nb(10))
    10101010101010101010
    """
    txt = ""
    for _ in range(n):
        txt = txt + str(n)
    return txt


def n_nb_2(n):
    """
    >>> print(n_nb_2(4))
    4;4;4;4;
    >>> print(n_nb_2(1))
    1;
    >>> print(n_nb_2(10))
    10;10;10;10;10;10;10;10;10;10;
    """
    txt = ""
    for _ in range(n):
        txt = txt + str(n) + ";"
    return txt


def n_nb_3(n):
    """
    >>> print(n_nb_3(4))
    ;4;4;4;4
    >>> print(n_nb_3(1))
    ;1
    >>> print(n_nb_3(10))
    ;10;10;10;10;10;10;10;10;10;10
    """
    txt = ""
    for _ in range(n):
        txt = txt + ";" + str(n)
    return txt


def n_nb_4(n):
    """
    >>> print(n_nb_4(4))
    4;4;4;4
    >>> print(n_nb_4(1))
    1
    >>> print(n_nb_4(10))
    10;10;10;10;10;10;10;10;10;10
    """
    return n_nb_2(n)[:-1]



#EX3
def compte_avant(n):
    """
    >>> print(compte_avant(4))
    1234
    >>> print(compte_avant(10))
    12345678910
    """
    txt = ""
    for i in range(1, n+1):
        txt = txt + str(i)
    return txt


def compte_rebours(n):
    """
    >>> print(compte_rebours(4))
    43210
    >>> print(compte_rebours(10))
    109876543210
    """
    txt = ""
    for i in range(0, n+1):
        txt = str(i) + txt
    return txt


def compte_rebours_2(n):
    """
    >>> print(compte_rebours_2(4))
    4...3...2...1...0!
    >>> print(compte_rebours_2(10))
    10...9...8...7...6...5...4...3...2...1...0!
    """
    txt = ""
    for i in range(1, n+1):
        txt = str(i) + '...' + txt
    return txt + '0!'




#EX4
def binaire_4(n):
    """
    >>> print(binaire_4(8))
    (ERREUR)
    >>> print(binaire_4(4))
    (ERREUR)
    >>> print(binaire_4(-1))
    (ERREUR)
    >>> print(binaire_4(0))
    00
    >>> print(binaire_4(2))
    10
    """
    if n > 3 or n < 0:
        return "(ERREUR)"
    if n == 0:
        return "00"
    if n == 1:
        return "01"
    if n == 2:
        return "10"
    if n == 3:
        return "11"


def nombre_bits(n):
    """
    >>> print(nombre_bits(-42))
    (ERREUR)
    >>> print(nombre_bits(0))
    1
    >>> print(nombre_bits(2))
    2
    >>> print(nombre_bits(6))
    3
    >>> print(nombre_bits(42))
    6
    >>> print(nombre_bits(1024))
    11
    >>> print(nombre_bits(2**100 - 1))
    100
    >>> print(nombre_bits(2**100))
    (ERREUR)
    """
    if n < 0:
        return '(ERREUR)'

    bits = 0
    for _ in range(100):
        if n < 2:
            return bits + 1
        n = n // 2
        bits += 1

    return '(ERREUR)'


def binaire(n):
    """
    >>> binaire(-42)
    '(ERREUR)'
    >>> binaire(0)
    '0b0'
    >>> binaire(1)
    '0b1'
    >>> binaire(4)
    '0b100'
    >>> binaire(6)
    '0b110'
    >>> binaire(42)
    '0b101010'
    >>> binaire(2**100)
    '(ERREUR)'
    """
    if n < 0:
        return '(ERREUR)'
    
    res = ""
    n_bits = nombre_bits(n)
    if n_bits == '(ERREUR)':
        return '(ERREUR)'

    for i in range(nombre_bits(n)):
        b = n % 2
        res = str(b) + res
        n = n // 2
    return '0b' + res








if __name__ == '__main__':
    testmod()
