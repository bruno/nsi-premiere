def somme(carre : list, n : int) -> ?:
    """carre est tableau carré de nombre
    n est un nombre entier"""
    somme = 0
    for nombre in carre[n]:
        somme = somme + nombre
    return somme