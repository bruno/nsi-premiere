def frequence(mot : str, lettre : str) -> float :
    compteur = 0
    for l in mot :
        if lettre == l :
            compteur = compteur + 1
    return compteur / len(mot)

def total_str1(n : int) -> str :
    return f"total: {n}"

def total_str2(n : int) -> str :
    return "total: " + str(n)

def somme1(a : int, b : int) -> str :
    return f"{a}+{b}={a+b}"

def somme2(a : int, b : int) -> str :
    return str(a)+"+"+str(b)+"="+str(a+b)
