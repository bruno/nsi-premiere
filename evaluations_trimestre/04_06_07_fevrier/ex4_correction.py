def egalite_lignes1(carre : list) -> bool :
    n = 0
    somme = somme(carre, n)
    while n < len(carre) - 1 :
        if somme != somme(carre, n + 1):
            return False
    return True

def egalite_lignes(carre : list) -> bool :
    reference = somme(carre, 0)
    for i in range(1, len(carre)) :
        if reference != somme(carre, i):
            return False
    return True