def nombres_de_cartes(n : int) -> int :
    """calcule le nombre de cartes nécessaires
       à la fabrication d'un chateau comportant n étages"""
    somme = 0
    for i in range(1,n+1) :
        somme = somme + 3*i - 1
    return somme

assert nombres_de_cartes(0) == 0
assert nombres_de_cartes(4) == 26

def nombre_etage_max(n:int) -> int :
    """calcule le nombre d'étages d'un chateau
    qu'il est possible de construire avec n cartes"""
    etages = 0
    while n >= nombres_de_cartes(etages + 1) :
        etages = etages + 1
    return etages

assert nombre_etage_max(52) == 5
assert nombre_etage_max(1) == 0