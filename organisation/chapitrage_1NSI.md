Thèmes :

- Données1 : Représentation des données : types et valeurs de base
- Données2 : Représentation des données : types construits
- Données3 : Traitement de données en tables
- Machines1 : Interactions entre l’homme et la machine sur le Web
- Machines2 : Architectures matérielles et systèmes d’exploitation
- Langages : Langages et programmation
- Algorithmes : Algorithmique

Nombre de semaines :

- Rentrée (36) -> Automne (43) : 7
- Automne (45) -> Fin année (52) : 7
- Fin année (2) -> Hiver (9) : 7
- Hiver (11) -> Printemps (17) : 6
- Printemps (19) -> Été (24) : 5

Total : 32


| Chapitre | Durée (sem)| Trimestre |
|:---|:---:|:---:|
| Découverte de la science informatique | 1 | 1 | # info débranchée (formalisme), évaluation du niveau en programmation
| Machines2/Modèle d'architecture séquentielle | 2 | 1 | # machine à registre
| Données1/Expressions booléennes | 1 | 1 |
| Données1/Écriture d’un entier positif dans une base b⩾2 | 1 | 1 |
| Langages/Constructions élémentaires(affectation, type, condition, boucles) | 2 | 1 |
| Données1/Représentation relatifs et réels | 2 | 1 | # on s'appuie sur des problèmes vus au chapitre précédent
| Langages/Constructions élémentaires(fonctions) | 2 | 1 |
| Machines2/IHM (info embarquée) | 1 | 1 |
| Machines2/Systèmes d'exploitation |||
| Machines2/Architecture réseau |||
| Machines1/Pages web |||
| Données1/Encodage texte |||
| Données2/Tableau indexé | 2 | 2 |
| Algorithmes/Parcours d'un tableau |||
| Algorithmes/Tris d'un tableau |||
| Données2/Dictionnaires|||
| Données3/Indexation de tables|||
| Données3/Recherche et tri de tables|||
| Données3/Fusion de tables|||
| Machines2/Interactions client-serveur |||
| Langages/Bonnes pratiques de programmation | | |
| Algorithmes/Algorithmes avancés |||
||||


