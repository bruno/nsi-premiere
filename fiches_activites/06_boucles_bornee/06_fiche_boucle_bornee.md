---
title: "Constructions élémentaires : boucles bornées"
author: ""
date: "2023-11-13"
subject: "ConstructionsElementaires"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...


## Activité 1

**Consigne** :

Écrire en pseudo-code et en python un algorithme qui demande à l’utilisateur un nombre entier `n`, 
et qui affiche les `n` premiers entiers.

-----

## Activité 2

**Consigne** :

Écrire en pseudo-code et en python un algorithme qui demande à l'utilisateur un nombre réel
(flottant) puis un nombre entier et effectue la multiplication de ces nombres **sans** utiliser 
l'opérateur de multiplication.

-----

## Activité 3

**Consigne** :

Écrire un programme en python permettant de dessiner la spirale ci-dessous:

![spirale](res/prog-spirale.png){width=4cm}

::: info  
Le module `turtle` permet de tracer des demi-cercle avec l'instruction `circle(diamètre, angle)`, 
la modification de l'épaisseur du trait se fait quant à elle avec l'instruction `width(épaisseur)`.  
L'instruction ci-dessous paramètre l'épaisseur du trait à 2 puis trace un demi-cercle (angle = 180°) 
de diamètre 10; ce qui peut constituer la première étape de construction de notre spirale.
:::


```python
from turtle import width, circle    #importation des fonctionnalités nécessaires

width(2)
circle(10, 180)
```
