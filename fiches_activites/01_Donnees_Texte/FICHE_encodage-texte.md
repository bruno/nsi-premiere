---
title: "Représentation des données : Encodage du texte"
author: 
date: "février 2024"
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
...

## Activité : le message codé de Perseverance

**Situation** :

Le rover Perseverance a été largué sur la planète Mars le 18 février 2021.

Dans le parachute était inscrit un message codé.

|![](parachute_perseverance.png){width=300px} | ![](parachute_perseverance2.png){width=300px} |
|:---:|:---:|
| photo du parachute | schéma |

**Consignes**

 1. Décoder le message à l'aide des indices ci-dessous.

*Indices* :

 - Une séquence de 3 bandes blanches sépare le code de chaque caractère.
 - Le sens de lecture est le sens horaire.
 - Le message commence au centre du parachute.
 - La première lettre est "D".
 - Le dernier "étage" du message donne les coordonnées du larguage.

2. Le codage utilisé est ambiguë, expliquer pourquoi.


3. Proposer un codage permettant de lever les ambiguités.