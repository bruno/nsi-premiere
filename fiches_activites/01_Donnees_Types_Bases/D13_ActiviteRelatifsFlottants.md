---
title: "1NSI : Représentation des entiers relatifs et des nombres réels"
author: 
# date: "2022-09-20"
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
  
...

# Activité : représentation approximative d'un nombre flottant

### 1. Que renvoie le texte `0.1 + 0.2 == 0.3` saisi dans la console Python ?

$\dotfill$

### 2. Saisir à présent `0.1 + 0.2`. Que remarquez-vous ?  

$\dotfill$

Nous allons essayer de comprendre pourquoi la console Python réagit ainsi.  


## Décomposition d'un nombre flottant en base décimale

### 3. Remplir le tableau ci-dessous avec le nombre flottant $32,1675_{10}$ puis le décomposer en base décimale  

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|} \hline
    $10^1$ & $10^0$ & $10^{-1}$ & $10^{-2}$ & $10^{-3}$ & $10^{-4}$ \\ \hline
    \dots & \dots & \dots & \dots & \dots & \dots \\ \hline
\end{tabular}
\end{center}

$32,1675_{10} = \dotfill$

## Conversion d'un nombre flottant en base binaire

### 4. Remplir le tableau ci-dessous avec le nombre flottant $10,1011_{2}$ puis le décomposer en base binaire  

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|} \hline
    $2^1$ & $2^0$ & $2^{-1}$ & $2^{-2}$ & $2^{-3}$ & $2^{-4}$ \\ \hline
    \dots & \dots & \dots & \dots & \dots & \dots \\ \hline
\end{tabular}
\end{center}


$10,1011_{2} = \dotfill$


### 5. Convertir $0,25_{10}$ en base binaire.

> **Méthode de conversion d'une partie décimale d'un nombre flottant vers la base binaire**  
> 1. Multiplier par 2 la partie décimale.  
> 2. Relever la partie entière comme prochain chiffre après la virgule.  
> 3. Garder la partie décimale du produit.    
> 4. Recommencer avec cette nouvelle valeur tant que celle-ci n'est pas nulle.  

\begin{tabular}{|c|c|c|} \hline
    Partie décimale × 2 & Partie entière du produit & Partie décimale du produit \\ \hline
    $0,25 \times 2$ & \dotfill & \dotfill \\ \hline
    \dotfill & \dotfill & \dotfill \\ \hline
\end{tabular}


$0,25_{10} = \dotfill$

### 6. Compléter le tableau suivant pour calculer les 6 premières étapes de la conversion de $0,1_{10}$ en base binaire.

\begin{tabular}{|c|c|c|} \hline
    Partie décimale × 2 & Partie entière du produit & Partie décimale du produit \\ \hline
    $0,1 \times 2$ & \dotfill & \dotfill \\ \hline
    \dotfill & \dotfill & \dotfill \\ \hline
    \dotfill & \dotfill & \dotfill \\ \hline
    \dotfill & \dotfill & \dotfill \\ \hline
    \dotfill & \dotfill & \dotfill \\ \hline
    \dotfill & \dotfill & \dotfill \\ \hline
\end{tabular}


Que remarquez-vous ? $\dotfill$

### 7. Sachant qu'une machine ne peut stocker qu'un nombre fini de bits dans sa mémoire, l'écriture en machine de $0,1_{10}$ est-elle exacte ?

$\dotfill$

### 8. Expliquer alors les réponses aux question 2 et 3.

$\dotfill$

$\dotfill$

### 9. En programmation est-il pertinent de tester une égalité entre deux nombres flottants ?

$\dotfill$

## Contourner les problèmes de test d'égalité avec les flottant

### 10. Saisir les instructions ci-dessous dans une console Python.

```python
>>> from math import *
>>> isclose(0.1+0.2, 0.3)
```
Que permet la fonction `isclose()` ?

### 11. Utiliser la fonction `abs()`.

On peut aussi utiliser la fonction `abs()` pour vérifier que la valeur absolue d'une différence entre deux nombres
est négligeable. Par exemple `abs(a-b)<=10**-9` renvoie True si la différence entre a et b est inférieure à `0,000 000 001`.

Écrire le test de la question 2, en utilisant la fonction `abs()` : $\dotfill$ .