---
title: "Structures de données : Interface et implémentations"
author: "d'après Germain BECKER, Lycée Mounier, ANGERS"
date: "2023-09-04"
subject: "StructureDonnées"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...

## Activité : Composants d'un ordinateur

Lorsqu'on ouvre un ordinateur, quelle que soit sa marque, nous retrouvons en général toujours les mêmes composants : le processeur ou microprocesseur, la mémoire vive (RAM), la carte mère, la carte graphique, interface de connexion des périphériques, le lecteur de disque, le disque dur, le clavier, l'écran, la souris, l'alimentation électrique.

**Question 1** : Associer le bon élément à chaque numéro du schéma ci-dessous.

![Les composants d'un ordinateur](Personal_computer.png){width=400px}

\newpage

**Question 2** : Associer chaque élément à sa description

- Cerveau de l'ordinateur, qui permet à l'ordinateur d'effectuer les 
opérations (calculs) demandés :

::: box

:::

- Stocke les informations des programmes et données en cours de fonctionnement :  

::: box

:::

- Relie tous les éléments constituant un ordinateur. Sa principale fonction est 
la mise en relation de ces composants par des bus sous forme de circuits imprimés :  

::: box

:::

- Carte d’extension d'ordinateur qui permet de produire une image affichable 
sur un écran : 

::: box

:::

- Périphérique d’entrée qui permet d’écrire :   

::: box

:::

- Assure la mise sous tension de l'ensemble des composants 	:  
 
::: box

:::
 
- Périphérique d'entrée-sortie qui stocke les données de base de la machine :  
  
::: box

:::
	
- Périphérique d'entrée-sortie, assure le stockage et la lecture de données 
  sur support externe non volatile 	:  

::: box

:::

- Périphérique d’entrée qui permet de déplacer le curseur de pointage à l'écran :  
  
::: box

:::

- Permet de connecter les périphériques (disque dur, lecteur DVD, etc.) à la carte
  mère :  

::: box

:::

- Périphérique de sortie qui permet de visualiser les informations venant de
  l'ordinateur :  


::: box

:::
