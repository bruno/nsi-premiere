def parite_somme(tab : list[int]) -> bool :
    somme = 0
    for valeur in tab :
        somme = somme + valeur
    return somme % 2 == 0