"""
author: Pascal Padilla
permet la conversion de points de codes en codes utf32, utf16 et utf8
"""

from doctest import testmod

def padding(n: str, length: int) -> str:
    """Renvoie le nombre n avec un padding de 0 à gauche.

    Parameters
    ----------
    n : (str) nombre sous forme de chaîne à compléter
    length : (int) longueur finale

    Returns
    -------
    (str) chaine de taille length égale à n avec un padding

    Examples
    --------
        >>> padding('41', 4)
        '0041'
        >>> padding('3a9', 4)
        '03a9'
        >>> padding('3a9', 8)
        '000003a9'
        >>> padding('6587', 4)
        '6587'
        >>> padding('6587', 8)
        '00006587'
    """
    longueur = len(n)
    while longueur < length:
        n = '0' + n
        longueur += 1
    return n


def _concat(head: str, queue: str, n: int) -> str:
    """ Concatène head avec queue pour produire un nombre de n bit
    en hexadécimal.

    Parameters
    ----------
    head : (str) chaine de caractère en binaire
    queue : (str) chaine de caractère en binaire
    n : (int) longueur en bit du nombre renvoyé

    Returns
    -------
    (str) concaténation au format hexadécimal

    Examples
    --------
        >>> a = _concat('110110', '1111', 16)
        >>> b = str(hex(int('000001101101111',2)))
        >>> a == b[2:]
        True
    """
    code = head + queue
    code = hex(int(code,2))
    code = code[2:]
    code = padding(code, n//8)
    return code


def point_de_code_to_utf32(n: int) -> str:
    """
    Entrées
    -------
    n (int): point de code en hexadécimal
    
    Tests
    -----
        >>> point_de_code_to_utf32(0x0041)
        '00000041'
        >>> point_de_code_to_utf32(0x03a9)
        '000003A9'
        >>> point_de_code_to_utf32(0x6587)
        '00006587'
        >>> point_de_code_to_utf32(0x10384)
        '00010384'
    """
    res: str = str(hex(n))
    res = res[2:]
    n = len(res)
    return padding(res,8).upper()


def point_de_code_to_utf16(n: int) -> str:
    """ Convertit un point de code Unicode en code UTF-16

    Parameters
    ----------
    n : (int) point de code en hexadécimal

    Returns
    -------
    (str) code(s) en UTF-16

    Examples
    --------
        >>> point_de_code_to_utf16(0x0041)
        '0041'
        >>> point_de_code_to_utf16(0x03a9)
        '03A9'
        >>> point_de_code_to_utf16(0x6587)
        '6587'
        >>> point_de_code_to_utf16(0x10384)
        'D800 DF84'
    """
    code = str(hex(n))
    code = code[2:]
    if len(code) < 5:
        return padding(code.upper(), 4)
    
    code = bin(n)[2:]
    #  000y~yyyy~xxxx~xxxx~xxxx~xxxx 
    code = padding(code,24)
    x:str = code[8:]
    y:str = code[3:8]  # yyyyy
    z = bin(int(y,2) - 1)
    z = padding(z[2:], 4)  # zzzz = yyyyy - 1

    # 1101~10zz~zzxx~xxxx 1101~11xx~xxxx~xxxx
    code1 = _concat('110110',z+x[:6],16)
    code2 = _concat('110111',x[6:],16)
    code = code1 + " " + code2
    return code.upper()



def point_de_code_to_utf8(n: int) -> str:
    """ Convertit un point de code Unicode en code UTF-16

    Parameters
    ----------
    n : (int) point de code en hexadécimal

    Returns
    -------
    (str) code(s) en UTF-8

    Examples
    --------
        >>> point_de_code_to_utf8(0x0041)
        '41'
        >>> point_de_code_to_utf8(0x03a9)
        'CE A9'
        >>> point_de_code_to_utf8(0x6587)
        'E6 96 87'
        >>> point_de_code_to_utf8(0x10384)
        'F0 90 8E 84'
    """
    code = str(hex(n))
    code = code[2:]
    if len(code) < 3:
        return padding(code.upper(), 2)
    elif len(code) < 4:
        # 0000~0yyy~yyxx~xxxx
        # => 110yyyyy 10xxxxxx
        code = bin(n)[2:]
        code = padding(code,16)
        x:str = code[10:]  # xxxxxx
        y:str = code[5:10]  # yyyyy
        code1 = _concat('110', y, 8)
        code2 = _concat('10', x, 8)      
        code = code1 + " " + code2
        return code.upper()
    elif len(code) < 5:
        # zzzz~yyyy~yyxx~xxxx
        # 1110zzzz 10yyyyyy 10xxxxxx
        code = bin(n)[2:]
        code = padding(code,16)
        z = code[:4]
        code_1 = _concat('1110', z, 8)
        y = code[4:10]
        code_2 = _concat('10', y, 8)
        x = code[10:]
        code_3 = _concat('10', x, 8)
        return f"{code_1} {code_2} {code_3}".upper()

    # 000u~uuuu~zzzz~yyyy~yyxx~xxxx 
    # 1111~0uuu 10uu~zzzz 10yy~yyyy 10xx~xxxx
    code = bin(n)[2:]
    code = padding(code,24)
    u = code[3:8]
    z = code[8:12]
    y = code[12:18]
    x = code[18:]
    code_1 = _concat('11110', u[:3], 8)
    code_2 = _concat('10', u[3:]+z, 8)
    code_3 = _concat('10', y, 8)
    code_4 = _concat('10', x, 8)
    return f"{code_1} {code_2} {code_3} {code_4}".upper()



print(point_de_code_to_utf8(0x03b5))

testmod()