def recherche(L,valeur):
    trouve = False
    i = 0
    while not trouve and i < len(L) :
        if L[i] == valeur :
            trouve = True
        i = i + 1
    return trouve

def search(tab : list) -> int :
    m = 0
    for i in range(1,len(tab)) :
        if tab[i] < tab[m] :
            m = i
    return m

