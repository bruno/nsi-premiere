a = [10,20,30]
b = a
b[2] = 1

impairs = [1, 3, 5, 7, 9]

impairs = []
for n in range(5):
    impairs.append(2 * n + 1)

impairs = [2 * n + 1 for n in range(5)]

impairs = [n for n in range(1, 11 ,2)]

impairs = []
n = 0
while len(impairs) != 5:
    if n % 2 == 1:
        impairs.append(n)
    n = n + 1