from capytale.autoeval import ValidateVariables, ValidateFunction, ValidateFunctionPretty
# contenu de _exercices.py
import random
from capytale.random import user_seed
from IPython.display import Markdown


def consigne_exercice1():
    random.seed(a=user_seed())
    a = random.randint(-15, -2)
    b = random.randint(1, 15)
    print(a,b)
    return Markdown(f"> **Exercice :** dans la cellule ci-dessous, implémenter en Python la fonction $f$ définie par $f(x) = {a}x + {b}$.")


def correction_exercice1():
    # l'initialisation puis les appels au générateur aléatoire
    # exactements identiques garantissent que les mêmes valeurs
    # sont générées
    random.seed(a=user_seed())
    a = random.randint(-15, -2)
    b = random.randint(1, 15)
    return Markdown(f"""\
> Il fallait implémenter en Python la fonction $f$ définie par $f(x) = {a}x + {b}$.
> On peut faire comme ceci :

```python
def f(x):
    return {a} * x + {b}
```
""")

test_variable_c1 = ValidateVariables({"c1" : 5})
test_variable_c2 = ValidateVariables({"c2" : 5**2 + 3})
test_variable_c3 = ValidateVariables({"c3" : 3*(8 - 6) / (5**2 - 12**3)})



valeurs = range(-10,10)
test_fonction_carre = ValidateFunction("carre", test_values = valeurs, target_values = [x**2 for x in valeurs])
test_carre = ValidateFunction("carre", test_values=[-2, -1, 0, 1, 2], target_values=[4, 1, 0, 1, 4])
test_valeur_absolue = ValidateFunction("valeur_absolue", test_values=[-2, -1.5, 0, 1.1, 2], valid_function=abs)
values = [(True, True), (True, False), (False, True), (False, False)]
test_xor = ValidateFunctionPretty("xor", values, target_values=[x != y for x, y in values])


test_variable_c1 = ValidateVariables({"c1" : 5})

values = [(True, True), (True, False), (False, True), (False, False)]
test_xor = ValidateFunctionPretty("xor", values, target_values=[x != y for x, y in values])

def _abs(x: float) -> float:
    return x if x > 0 else -x

test_valeur_absolue = ValidateFunction(
    "valeur_absolue", [x / 10 for x in range(-20, 21)],
    valid_function=_abs, check_signature=True)
