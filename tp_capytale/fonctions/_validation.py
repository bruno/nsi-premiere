from basthon.notebook import ValidationFunction

class MyValidation(ValidationFunction):
    def handle_failure(self, value, target, result):
        print(f"❌ Ta fonction ne renvoie pas le bon résultat pour la valeur {value}.")
        return False

    def handle_exception(self, exc, value, target):
        print(f"❌ Ta fonction a rencontré un problème pour la valeur {value} :")
        return super().handle_exception(exc, value, target)

    def handle_full_success(self):
        print("✅ Félicitations ! Ton code fonctionne.")


class MyValidationEnd(ValidationFunction):
    def handle_full_success(self):
        print("✅ Félicitations ! Tu as terminé le notebook !")


test_oppose = MyValidation("oppose", [10, -1.1, 2e5], [-10, 1.1, -2e5])
test_carre = MyValidation("carre", [2, -3, 0], [4, 9, 0])
test_est_rectangle = MyValidation("est_rectangle",  [(3,4,5), (17, 15, 8), (6,7,8) ], [True, True, False])
test_max2 = MyValidation("max2", [(1, 2), (-20, 0), (11, 11), (45, 44)], [2, 0, 11, 45])
test_max3 = MyValidation("max3", [(1, 2, 3), (-20, -5, 0), (11, 11, 11), (45, 43, 44)], [3, 0, 11, 45])
test_puissance = MyValidation("puissance", [(10,0), (2,3), (-2,3), (3,4)], [1, 8, -8, 81])
test_bissextile = MyValidation("bissextile", [2004, 2000, 1900], [True, True, False])
test_nbjoursannee = MyValidationEnd("nbjoursannee", [2004, 1999, 1900], [366, 365, 365])