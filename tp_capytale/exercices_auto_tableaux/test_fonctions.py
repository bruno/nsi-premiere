def suff(tab1 : list, tab2 : list) -> bool :
    l1 = len(tab1)
    i2 = len(tab2) - l1
    for i in range(i2, len(tab2)):
        if tab1[i-i2] != tab2[i] :
            return False
    return True


def _echange(tab : list, i : int, j : int) -> list :
    t = tab[:]
    a = t[i]
    b = t[j]
    t[i] = b
    t[j] = a
    return t

values_echange = [([1,3,4], 0, 2), (["a","b","c""d"],3,1)]

print(_echange([1,3,4], 0, 2))