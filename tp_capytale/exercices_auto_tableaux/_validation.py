from capytale.autoeval import  ValidateFunction, ValidateFunctionPretty
import random
from capytale.random import user_seed
from IPython.display import Markdown

random.seed(a=user_seed())

### Exercice 0 ###
tableaux_copie = [[random.randint(1,100) for _ in range(random.randint(1,10))] for _ in range(5)]

def _copie(t : list) -> list :
    return t[:]

test_copie = ValidateFunctionPretty("copie", 
    tableaux_copie, valid_function = _copie, check_signature=True)

### Exercice 1###

def _indices_pair(tab: list) -> list:
    return [tab[i] for i in range(len(tab)) if i%2==0]

test_indices_pair = ValidateFunctionPretty(
    "indices_pair", [[0,1,2,3,4],[1,1,1,1],[],[10,9,8,7,6,5]],
    valid_function=_indices_pair, check_signature=True)

### Exercice 2###

tableaux_occ = [[random.randint(1,10) for _ in range(20)] for _ in range(5)]
values_occ = [(random.randint(1,10),tab) for tab in tableaux_occ]

def _occ(valeur : int, tab : list) -> int :
    nb_occurence  = 0
    for element in tab :
        if element == valeur :
            nb_occurence += 1
    return nb_occurence

test_occurence = ValidateFunctionPretty("occurence",
    values_occ, valid_function = _occ, check_signature=True)

### Exercice 3 ###

tableaux_notes = [[random.randint(0,40)*0.5 for _ in range(random.randint(1,10))] for _ in range(5)]

def _moy(notes : list) -> float :
    somme = 0
    for note in notes :
        somme += note
    return somme/len(notes)

test_moyenne = ValidateFunctionPretty("moyenne",
    tableaux_notes, valid_function = _moy, check_signature=True)

### Exercice 4 ###

def _fibo(n : int) ->list :
    assert n > 2
    tab = [0,1]
    for i in range(2,n):
        tab += [tab[i-1] + tab[i-2]]
    return tab

values_fibo = [3, 4, 5, 6, 7, 8, 9, 10]
test_fibonacci = ValidateFunctionPretty("fibonacci",
    values_fibo, valid_function = _fibo, check_signature=True)

### Exercice 5 ###

def _ajout(c : str, t : list) -> list :
    return [c] + t

values_ajout =[("a", ["b","c","d"]), ("début" ,["et","fin"]),("tout",["est","bien"])]

test_ajout = ValidateFunctionPretty("ajout",
    values_ajout, valid_function = _ajout, check_signature=True)

### Exercice 6 ###

def _echange(tab : list, i : int, j : int) -> list :
    t = tab[:]
    a = t[i]
    b = t[j]
    t[i] = b
    t[j] = a
    return t

values_echange = [([1,3,4], 0, 2), (["a","b","c","d"],3,1)]

test_echange = ValidateFunctionPretty("echange",
    values_echange, valid_function = _echange, check_signature=True)

### Exercice 7 ###

def _pref(tab1 : list, tab2 : list) -> bool :
    i = 0
    while i < len(tab1) :
        if tab1[i] != tab2[i] :
            return False
        i += 1
    return True

values_pref = [([1],[1,2,3]),([0],[1,2,3]),([1,2,3],[1,2,3,4,5]),([2,3],[1,2,3,4,5]),(["a","b","c"],["a","b","c","d","e"])]

test_prefixe = ValidateFunctionPretty("prefixe",
    values_pref, valid_function = _pref, check_signature=True)

### Exercice 8 ###

def _suff(tab1 : list, tab2 : list) -> bool :
    l1 = len(tab1)
    i2 = len(tab2) - l1
    for i in range(i2, len(tab2)):
        if tab1[i-i2] != tab2[i] :
            return False
    return True

values_suff = [([2,3,4],[1,2,3,4]),([2,3],[1,2,3,4]),([2,3,4],[2,3]),(["e"],["a","b","c","d","e"])]


test_suffixe = ValidateFunctionPretty("suffixe",
    values_suff, valid_function = _suff, check_signature=True)


### Exercice 9 ###

def _mini(tab : list) -> int :
    i_min = 0
    for i in range(1, len(tab)):
        if tab[i] < tab[i_min] :
            i_min = i
    return i_min

values_mini = [[random.randint(0,20) for _ in range(random.randint(10,30))] for _ in range(10)]

test_mini = ValidateFunctionPretty("mini",
    values_mini, valid_function = _mini, check_signature=True)