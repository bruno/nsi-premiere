from capytale.autoeval import ValidateVariables
import random
from capytale.random import user_seed
from IPython.display import Markdown

random.seed(a=user_seed())

test_variable_v1 = ValidateVariables({"v1" : 1.5})
test_variable_v2 = ValidateVariables({"v2" : 8**2 - 4.2})
test_variable_v3 = ValidateVariables({"v3" : 2*1.5 - (8**2 - 4.2) })
test_variable_v4 = ValidateVariables({"v4" : "alala" })

x = random.randint(-21,-5)
def affiche_x():
    return Markdown(f"$x$ = {x}")

test_variable_v5 = ValidateVariables({"v5" : 6*x**3 - 0.5*x**2 + 2 })
test_variable_v6 = ValidateVariables({"v6" : int(13/7) })
