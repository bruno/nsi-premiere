from capytale.autoeval import  ValidateVariables, ValidateFunction, ValidateFunctionPretty
import random
from capytale.random import user_seed
from IPython.display import Markdown

random.seed(a=user_seed())

### Exercice 1

test_mon_dico = ValidateVariables({"mon_dico": {"device": "laptop" , "constructeur": "dell" , "ram": "8G" , 
"processeur": "Intel core i5", "stockage": "750 G", "Système d'exploitation" : "Debian 11"} })

### Exercice 2

def _dico_carres(n : int) -> dict :
    return {i:i**2 for i in range(1,n+1)}

test_dico_carres = ValidateFunctionPretty("dico_carres",
    [random.randint(1,20) for _ in range(5)],
    valid_function=_dico_carres, check_signature=True)

### Exercice 3

def _dico_pair_impair(l: list) -> dict :
    d = {}
    for n in l :
        if n%2 == 0 :
            d[n] = "pair"
        else :
            d[n] = "impair"
    return d

tableaux_entiers = [[random.randint(0,30) for _ in range(random.randint(5,10))] for _ in range(5)]

test_dico_pair_impair = ValidateFunctionPretty("dico_pair_impair",
    tableaux_entiers, valid_function = _dico_pair_impair, check_signature=True)

### Exercice 4

def _conca_dico(d1 : dict, d2 : dict, d3 : dict) -> dict :
    d = {}
    for cle in d1 :
        d[cle] = d1[cle]
    for cle in d2 :
        d[cle] = d2[cle]
    for cle in d3 :
        d[cle] = d3[cle]
    return d

dicos = [({"HP": 11 , "Acer": 7 , "Lenovo": 17 , "Del": 23},{"Sumsung": 22 , "Iphone": 9 , "Other": 13 },
{"Sumsung": 15 , "Other": 13}),({1:2},{2:3},{3:4})]


test_conca_dico = ValidateFunctionPretty("conca_dico",
    dicos, valid_function = _conca_dico, check_signature=True)
