
def _dico_carres(n : int) -> dict :
    return {i:i**2 for i in range(1,n+1)}

print(_dico_carres(8))