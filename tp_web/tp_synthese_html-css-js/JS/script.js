function couleur(color) {
    //document.body.style.backgroundColor = color;
    let defi = document.getElementById("defi1");
    defi.style.backgroundColor = color;

}

var texte1 = "Lorem ipsum dolor sit amet, consectetur adipisci elit, \
sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, \
quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid \
ex ea commodi consequatur. ";

var texte2 = "Quis aute iure reprehenderit in voluptate velit esse cillum dolore \
eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in \
culpa qui officia deserunt mollit anim id est laborum";

function changeTexte(code) {
    let paragraphe = document.getElementById("pmodif");
    if (code === 1) {
        paragraphe.innerHTML = texte1
    }
    else if (code === 2) {
        paragraphe.innerHTML = texte2
    }
}

function incrementeCompteur() {
    let c = document.getElementById("compteur").textContent
    let valeur = parseInt(c)
    valeur ++
    document.getElementById("compteur").innerHTML = valeur
}

function razCompteur() {
    document.getElementById("compteur").innerHTML = 0
}

function calculeAge(annee) {
    return 2024 - annee
}

function afficheAge() {
    annee =  parseInt(document.getElementById('annee').value)
    age = calculeAge(annee)
    document.getElementById('repd4').innerHTML = "vous avez "+age+" ans"
}

function somme() {
    let entier1 = parseInt(document.getElementById('ent1').value)
    let entier2 = parseInt(document.getElementById('ent2').value)
    console.log(entier1)
    document.getElementById('repd5').innerHTML = entier1 + entier2
}