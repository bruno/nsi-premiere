---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "logo.png"
slideNumber: false
title: "Spécialité NSI - Simone Veil 2024"
AutresThemes: "Black (default) - White - League - Sky - Beige - Simple - Serif - Blood - Night - Moon - Solarized"
---

::: block
<small>L'**informatique** n'est pas plus la science des *ordinateurs* <br>
que l'**astronomie** n'est celle des *télescopes*.</small>{style=background:black;width:500px}
:::


---

# NSi

Numérique et Sciences Informatiques

<small>[Pascal Padilla](mailto:pascal.padilla@ac-aix-marseille.fr) --- Simone Veil 2024</small>

--

### Plan

1. contenu des enseignements
1. profil des élèves
1. poursuites d'études


---

#### Contenu des enseignements

# <br>
## NSi
# <br>

<!-- .slide: data-background="gif_quoi.gif" data-background-opacity=0.5 -->

--

<small>*Contenu des enseignements de NSi*</small>

#### spécialité classique 

### en première

* 4h / semaine
* cœf. 8 {.fragment .fade-in}
* BAC en contrôle continu {.fragment .fade-in}

--

<small>*Contenu des enseignements de NSi*</small>


#### spécialité classique 

### en terminale

* 6h / semaine
* ~~cœf. 8~~ {.fragment .fade-in}
* cœf. 16 {.fragment}
* ~~BAC en contrôle continu controle~~ {.fragment .fade-in}
* 2 épreuves de BAC en juin : {.fragment}
  * ⟶ 15 pts sur table
  * ⟶ 5 pts  sur ordinateur


--

<small>*Contenu des enseignements de NSi*</small>



* S**N**T ⟶ *numérique*
* NS**i** ⟶ sciences **informatiques**

--


<small>*Contenu des enseignements de NSi*</small>




* matière scientifique
* **toutes** les bases de l'informatique
  * machines
  * algorithmes
  * langages
  * données
  * interfaces


--

<small>*Contenu des enseignements de NSi*</small>



#### ICI au lycée Simone Veil :

* groupes réduits
* 1 ordinateur par élève
* cours et activités en lignes
  * seul et/ou par groupe
  * chacun son rythme
* projets par équipes
* challenges/concours


---


#### Profil des élèves

## pour qui ?

<!-- .slide: data-background="img_qui.jpg" data-background-opacity=0.7 -->


--


<!-- .slide: data-transition="slide" data-background="#b5533c" data-background-transition="zoom" -->


#### NSI... ce n'est PAS pour


* devenir un gamer pro
* apprendre à devenir un méchant pirate
* apprendre à réparer des ordinateurs
* devenir un pro de la bureautique


--


<!-- .slide: data-transition="slide" data-background="#4d7e65" data-background-transition="fade-in" -->

#### NSI... pour qui ?

Spécialité **scientifique**

* pour toutes et tous
* pas de prérequis mais attitudes exigeantes :
  * active
  * travailleuse
  * motivée
  * persévérante
  * précise


---


#### Poursuite d'études

## quels débouchés ?

<!-- .slide: data-background="img_apres.png" data-background-opacity=0.7 -->


--


### Les métiers

* ingénierie <small>(programmation, projets,..)</small>
* réseaux <small>(sécurité, système, défense,..)</small>
* recherche <small>(maths, informatique, médecine,..)</small>
* graphisme <small>(animation, jeux, 3D,..)</small>
* communication, santé, journalisme, etc...


--


### Les études

* BUT (informatique, multimédia/Internet, réseaux/télécom)
* école d'ingénieur (INSA, Polytech, etc..)
* Licence info., MPI, pluri-sciences éco, STAPS,..
* BTS production (cybersécurité, réseaux, )
* CPGE (classe prépa MP2I à Marseille, Cannes, etc.)
* écoles privées d'informatique (epitech, laplateforme.io, institut G4, etc..)



---


<!-- .slide: style="text-align: left;" -->

### MERCI

<small>
illustrations : Julie Van Grol <br>
<a href="https://www.julievangrol.com">https://www.julievangrol.com</a>
</small>
